export interface Player {
    id: number;
    avatar: string;
    name: string;
    license: string;
    jour1: boolean;
    jour2: boolean;
    jour3: boolean;
    events: string[];
    paiement: boolean;
}