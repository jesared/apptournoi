import { Player } from "@/types/players";



export const players: Player[] = [
        {
          id: 1,
          avatar: '/assets/images/noavatar.jpg', // Replace with actual avatar URL
          name: 'John Doe',
          license: '123456',
          jour1: true,
          jour2: false,
          jour3: false,
          events: ['A', 'C'],
          paiement: true
        },
        {
          id: 2,
          avatar: '/assets/images/noavatar.jpg', // Replace with actual avatar URL
          name: 'Jane Smith',
          license: '789012',
          jour1: true,
          jour2: false,
          jour3: false,
          events: ['B', 'F'],
          paiement: false
        },
        {
          id: 3,
          avatar: '/assets/images/noavatar.jpg', // Replace with actual avatar URL
          name: 'Jane Smith',
          license: '789012',
          jour1: false,
          jour2: true,
          jour3: false,
          events: ['A', 'D', 'G', "M"],
          paiement: true
        },
        {
          id: 4,
          avatar: '/assets/images/noavatar.jpg', // Replace with actual avatar URL
          name: 'Jane Smith',
          license: '789012',
          jour1: true,
          jour2: false,
          jour3: true,
          events: ['A', 'P'],
          paiement: true
        },
        {
          id: 5,
          avatar: '/assets/images/noavatar.jpg',
          name: 'Alice Johnson',
          license: '456789',
          jour1: false,
          jour2: true,
          jour3: true,
          events: ['B', 'D', 'F'],
          paiement: false
        },
        {
          id: 6,
          avatar: '/assets/images/noavatar.jpg',
          name: 'Bob Williams',
          license: '987654',
          jour1: true,
          jour2: true,
          jour3: false,
          events: ['C', 'E', 'G'],
          paiement: true
        },
        {
          id: 7,
          avatar: '/assets/images/noavatar.jpg',
          name: 'Charlie Davis',
          license: '654321',
          jour1: false,
          jour2: false,
          jour3: true,
          events: ['A', 'H', 'K'],
          paiement: true
        },
        {
          id: 8,
          avatar: '/assets/images/noavatar.jpg',
          name: 'David Wilson',
          license: '135790',
          jour1: true,
          jour2: true,
          jour3: true,
          events: ['B', 'O', 'M'],
          paiement: false
        },
        // Add more players as needed
      ];
      // Additional random player data






