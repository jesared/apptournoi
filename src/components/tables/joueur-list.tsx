"use client"

// components/PlayerList.tsx
import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import { RiContactsLine, RiDoorOpenLine, RiMoneyCnyBoxLine, RiMoneyEuroBoxLine, RiMoneyEuroCircleFill } from 'react-icons/ri';
import PlayerDetailsModal from '../modals/modal-detail-joueur';
import { players as exemplePlayers } from './exemple-joueurs';



const PlayerList= ({players = exemplePlayers}) => {

  

  
  const [selectedPlayer, setSelectedPlayer] = useState<number | null>(null);
  
   // Utilisez une fonction pour définir l'état initial des cases à cocher
   const initializeCheckboxStates = () => {
    const initialStates: { [key: string]: boolean } = {};
    players.forEach((player) => {
      initialStates[`jour1-${player.id}`] = player.jour1 || false;
      initialStates[`jour2-${player.id}`] = player.jour2 || false;
      initialStates[`jour3-${player.id}`] = player.jour3 || false;
    });
    return initialStates;
  };

  const [checkboxStates, setCheckboxStates] = useState<{ [key: string]: boolean }>(
    initializeCheckboxStates()
  );

   // Effet pour mettre à jour l'état local des cases à cocher lors des changements de données des joueurs
   useEffect(() => {
    const updatedStates: { [key: string]: boolean } = {};
    players.forEach((player) => {
      updatedStates[`jour1-${player.id}`] = player.jour1 || false;
      updatedStates[`jour2-${player.id}`] = player.jour2 || false;
      updatedStates[`jour3-${player.id}`] = player.jour3 || false;
    });
    setCheckboxStates(updatedStates);
    console.log('updatedStates', updatedStates)
  }, [players]);
  
  // const [checkboxStates, setCheckboxStates] = useState<{ [key: string]: boolean }>({});
  // const [checkboxStates, setCheckboxStates] = useState(initializeCheckboxStates);


  const showPlayerDetails = (playerId: number) => {
    setSelectedPlayer(playerId);
  };

  const closeModal = () => {
    setSelectedPlayer(null);
  };

  const handleCheckboxChange = (playerId: number, checkboxId: string) => {

    setCheckboxStates((prevStates) => ({
      ...prevStates,
      [`${checkboxId}-${playerId}`]: !prevStates[`${checkboxId}-${playerId}`],
    }));
  };

  
  return (
    <div className="container mx-auto my-8">
      <h2 className="text-2xl font-bold mb-4">Liste des participants</h2>
      <table className="min-w-full bg-white border border-gray-300 table">
        <thead>
          <tr className='text-center'>
            <th className="py-2 px-4 border-b">Avatar</th>
            <th className="py-2 px-4 border-b">Nom</th>
            <th className="py-2 px-4 border-b">Licence</th>
            <th className="py-2 px-4 border-b">Pointages</th>
            <th className="py-2 px-4 border-b">Epreuves</th>
            <th className="py-2 px-4 border-b">Paimements</th>
            <th className="py-2 px-4 border-b">Action</th>
          </tr>
        </thead>
        <tbody>
        {players && players.map((player) => (

            <tr key={player.id}>
              <td className="py-2 px-4 border-b text-center">
                <div className="flex items-center justify-center">
                  <Image width={48} height={48} src={player.avatar} alt="Avatar" className="rounded-full h-8 w-8 object-cover" />
                </div>
              </td>
              <td className="py-2 px-4 border-b text-center">{player.name}</td>
              <td className="py-2 px-4 border-b text-center">{player.license}</td>
              <td className="py-2 px-4 border-b text-center">
                <div className="space-x-2">
                  <input
                    type="checkbox"
                    id={`jour1-${player.id}`}
                    className="checkbox checkbox-secondary"
                    checked={checkboxStates[`jour1-${player.id}`] || false}
                    onChange={() => handleCheckboxChange(player.id, "jour1")}
                  />
                  <input
                    type="checkbox"
                    id={`jour2-${player.id}`}
                    className="checkbox checkbox-primary"
                    checked={checkboxStates[`jour2-${player.id}`] || false}
                    onChange={() => handleCheckboxChange(player.id, "jour2")}
                  />
                  <input
                    type="checkbox"
                    id={`jour3-${player.id}`}
                    className="checkbox checkbox-info"
                    checked={checkboxStates[`jour3-${player.id}`] || false}
                    onChange={() => handleCheckboxChange(player.id, "jour3")}
                  />
                </div>
              </td>
              <td className="py-2 px-4 border-b text-left">{player.events.join(', ')}</td>
              <td className="py-2 px-4 border-b text-center bg-neutral">
              <input type="checkbox" className="checkbox checkbox-success" />
              </td>
              <td className="py-2 px-4 border-b">
                <div className="flex items-center justify-center">
                  <button
                    className="btn btn-sm"
                    onClick={() => showPlayerDetails(player.id)}
                  >
                    <RiContactsLine className="text-lg" />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {/* Display player details based on the selectedPlayer state */}
      {selectedPlayer !== null && players && players.length > 0 && (

        <PlayerDetailsModal

          player={players.find((p) => p.id === selectedPlayer) || null}
          onClose={closeModal}
        />
      )}
    </div>
  );
};

export default PlayerList;
