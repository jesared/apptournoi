// components/PlayerDetailsModal.tsx
import { Player } from '@/types/players';
import React, { useState } from 'react';
import { RiCloseLine, RiEdit2Line } from 'react-icons/ri';

interface CheckboxState {
    jour1: boolean;
    jour2: boolean;
    jour3: boolean;
  }

interface PlayerDetailsModalProps {
  player: Player | null;
  onClose: () => void;
  checkboxState?: boolean;
}

const PlayerDetailsModal: React.FC<PlayerDetailsModalProps> = ({ player, onClose, checkboxState }) => {
  const [localCheckboxState, setLocalCheckboxState] = useState<CheckboxState>({
    jour1: false,
    jour2: false,
    jour3: false,
  });

  const handleCheckboxChange = (checkboxId: keyof CheckboxState) => {
    setLocalCheckboxState((prevState) => ({
      ...prevState,
      [checkboxId]: !prevState[checkboxId],
    }));
  };

  if (!player) {
    return null;
  }

  return (
    <div className="fixed inset-0 z-50 flex items-center justify-center">
      <div className="fixed inset-0 bg-black opacity-60"></div>
      <div className="bg-white p-8 rounded shadow-md relative z-10">
        <h2 className="text-2xl font-bold mb-4">Détails du joueur {player.id}</h2>
        <form>
          <div className="mb-4">
            <label htmlFor="playerName" className="block text-gray-700 text-sm font-bold mb-2">
              Nom:
            </label>
            <input
              type="text"
              id="playerName"
              name="playerName"
              value={player.name}
              className="w-full border border-gray-300 p-2 rounded"
              readOnly
            />
          </div>
          <div className="mb-4">
            <label htmlFor="playerLicense" className="block text-gray-700 text-sm font-bold mb-2">
              Numéro de licence:
            </label>
            <input
              type="text"
              id="playerLicense"
              name="playerLicense"
              value={player.license}
              className="w-full border border-gray-300 p-2 rounded"
              readOnly
            />
          </div>
          <div className="mb-4">
            <label htmlFor="pointage" className="block text-gray-700 text-sm font-bold mb-2">
              Pointage:
            </label>
            <div className="space-x-2">
              <input
                type="checkbox"
                id={`jour1-${player.id}`}
                className="checkbox checkbox-secondary"
                checked={localCheckboxState.jour1}
                onChange={() => handleCheckboxChange('jour1')}
              />
              <input
                type="checkbox"
                id="checkbox2"
                className="checkbox checkbox-primary"
                checked={localCheckboxState.jour2}
                onChange={() => handleCheckboxChange('jour2')}
              />
              <input
                type="checkbox"
                id="checkbox3"
                className="checkbox checkbox-info"
                checked={localCheckboxState.jour3}
                onChange={() => handleCheckboxChange('jour3')}
              />
            </div>
          </div>
          <div className="mb-4">
            <label htmlFor="playerEvents" className="block text-gray-700 text-sm font-bold mb-2">
              Epreuves:
            </label>
            <input
              type="text"
              id="playerEvents"
              name="playerEvents"
              value={player.events.join(', ')}
              className="w-full border border-gray-300 p-2 rounded"
              readOnly
            />
          </div>
        </form>
        <button
              type="submit"
              className="bg-primary hover:bg-secondary text-white font-bold py-2 px-4 rounded"
            >
              <RiEdit2Line className="text-2xl" />
            </button>
        <button
          onClick={onClose}
          className="absolute top-0 right-0 m-4 text-gray-600 hover:text-gray-800"
        >
          <RiCloseLine className="text-2xl" />
        </button>
      </div>
    </div>
  );
};

export default PlayerDetailsModal;