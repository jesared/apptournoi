// components/AddModal.js
import { useState } from 'react';
import { RiAddLine, RiCloseLine } from 'react-icons/ri';

interface AddModalProps {
    isOpen: boolean;
    onClose: () => void;
  }

const AddModal: React.FC<AddModalProps> = ({ isOpen, onClose }) => {
  const [formData, setFormData] = useState({
    playerName: '',
    playerPosition: '',
    licence:'',
  });

  const handleChange = (e: { target: { name: any; value: any; }; }) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    // Add your logic for handling the form submission here
    console.log('Form submitted:', formData);
    // Reset the form data
    setFormData({
      playerName: '',
      playerPosition: '',
      licence:','
    });
    // Close the modal
    onClose();
  };

  return (
    <div className={`fixed inset-0 z-50 ${isOpen ? 'block' : 'hidden'}`}>
      <div className="flex items-center justify-center min-h-screen">
        <div className="fixed inset-0 bg-black opacity-60"></div>
        <div className="bg-white p-8 rounded shadow-md relative z-10">
          <h2 className="text-2xl font-bold mb-4">Ajouter un joueur</h2>
          <form onSubmit={handleSubmit}>
            <div className="mb-4">
              <label htmlFor="playerName" className="block text-gray-700 text-sm font-bold mb-2">
                Nom:
              </label>
              <input
                type="text"
                id="playerName"
                name="playerName"
                value={formData.playerName}
                onChange={handleChange}
                className="w-full border border-gray-300 p-2 rounded"
                required
              />
            </div>
            <div className="mb-4">
              <label htmlFor="playerPosition" className="block text-gray-700 text-sm font-bold mb-2">
                Prénom:
              </label>
              <input
                type="text"
                id="playerPosition"
                name="playerPosition"
                value={formData.playerPosition}
                onChange={handleChange}
                className="w-full border border-gray-300 p-2 rounded"
                required
              />
            </div>
            <div className="mb-4">
              <label htmlFor="playerPosition" className="block text-gray-700 text-sm font-bold mb-2">
                Numéro de licence:
              </label>
              <input
                type="text"
                id="licence"
                name="licence"
                value={formData.licence}
                onChange={handleChange}
                className="w-full border border-gray-300 p-2 rounded"
                required
              />
            </div>
            <button
              type="submit"
              className="bg-primary hover:bg-secondary text-white font-bold py-2 px-4 rounded"
            >
              <RiAddLine className="text-2xl" />
            </button>
          </form>
          <button
            onClick={onClose}
            className="absolute top-0 right-0 m-4 text-gray-600 hover:text-gray-800"
          >
            <RiCloseLine className="text-2xl" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default AddModal;
