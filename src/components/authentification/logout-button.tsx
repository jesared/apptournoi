"use client"

import { signIn, signOut } from "next-auth/react";

const LogOutButton = () => {


    return (
        <>


            <button
                onClick={async () => {

                    await signOut();
                }}
                className="btn btn-primary"
            >LogOut</button>


        </>
    )
}

export default LogOutButton