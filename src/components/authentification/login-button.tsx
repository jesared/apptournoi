"use client"

import { signIn } from "next-auth/react";

const LogInButton = () => {

    


  return (
 
        <button
          onClick={async () => {
            await signIn();
          }}
          className="btn btn-primary"
        >Login</button>


 
  )
}

export default LogInButton