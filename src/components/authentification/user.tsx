import { authConfig } from "@/pages/api/auth/[...nextauth]";
import { getServerSession } from "next-auth";
import Image from "next/image"
import LogOutButton from "./logout-button";


export const User = async () => {

    const session = await getServerSession(authConfig);


    if (!session?.user) {
        return <p>no user</p>
    }

    return (
        <div className="card w-96 bg-base-100 shadow-xl">
            <div className="card-body">
                <div className="avatar">
                    <div className="w-24 rounded-full ring ring-primary ring-offset-base-100 ring-offset-2">
                        <Image width={48} height={48} alt="avatar" src={session.user.image ?? ''} />
                    </div>
                </div>
                <h2 className="card-title">{session.user.name}</h2>
                <p>{session.user.email}</p>
               <LogOutButton />

            </div>
        </div>
    )
}