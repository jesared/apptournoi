import { TableauxEngagementList as tabs } from "./tableaux"

const EpreuveContainer = () => {
  return (
    <div className="container mx-auto my-8">
    
    <h1 className="text-2xl font-bold mb-4">Liste des epreuves</h1>
      <table className="min-w-full bg-white border border-gray-300 table">
        <thead>
        <tr className='text-center'>
            <th className="py-2 px-4 border-b">Name</th>
            <th className="py-2 px-4 border-b">Date</th>
            <th className="py-2 px-4 border-b">Heure</th>
            <th className="py-2 px-4 border-b">Détail</th>
            <th className="py-2 px-4 border-b">Tarif</th>
        </tr>
        </thead>
        <tbody>
            {tabs && tabs.map((tab, index)=>(
                <tr key={tab.id} className={index % 2 === 1 ? "bg-base-200" : ""}>
                    <td className="py-2 px-4 border-b text-center">
                    {tab.name}
                    </td>
                    <td className="py-2 px-4 border-b text-center">
                    {tab.date}
                    </td>
                    <td className="py-2 px-4 border-b text-center">
                    {tab.heure}
                    </td>
                    <td className="py-2 px-4 border-b text-center">
                    {tab.detail}
                    </td>
                    <td className="py-2 px-4 border-b text-center">
                    {tab.tarif === "Gratuit" ? tab.tarif : `${tab.tarif} €`}
                    </td>
                </tr>
            )) }
        </tbody>    
    </table>
    
    </div>
  )
}

export default EpreuveContainer