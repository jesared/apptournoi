const Footer = () => {
    return (
      <footer className="bg-secondary-content text-white py-8">
        <div className="container mx-auto text-center">
          <p>&copy; 2023 Your Company. All rights reserved.</p>
          <div className="mt-4 flex justify-center space-x-4">
            <a href="#" className="text-gray-400 hover:text-gray-200">Privacy Policy</a>
            <a href="#" className="text-gray-400 hover:text-gray-200">Terms of Service</a>
            <a href="#" className="text-gray-400 hover:text-gray-200">Contact Us</a>
          </div>
        </div>
      </footer>
    );
  };
  
  export default Footer;