/* eslint-disable react/no-unescaped-entities */
'use client';


import Image from "next/image";
import Link from "next/link";
import { useEffect, useState, useRef } from "react";
import { RiUserAddLine, RiAccountCircleLine, RiLogoutCircleLine, RiHome2Line, RiHomeSmile2Line, RiTrophyLine, RiAccountPinCircleLine, RiLoginCircleLine } from "react-icons/ri";
import LogInButton from "../authentification/login-button";

interface NavbarProps {
  onAddClick: () => void;
  isUserLogin?: boolean;
}

const Navbar: React.FC<NavbarProps> = ({ onAddClick, isUserLogin = false }) => {

  const [isSubMenuOpen, setIsSubMenuOpen] = useState(false);
  const menuRef = useRef<HTMLDivElement>(null); // Specify the type as HTMLDivElement

  const handleAvatarClick = () => {
    setIsSubMenuOpen(!isSubMenuOpen);
  };
  const handleDocumentClick = (e: { target: any; }) => {
    if (menuRef.current && !menuRef.current.contains(e.target)) {
      setIsSubMenuOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleDocumentClick);

    return () => {
      document.removeEventListener('click', handleDocumentClick);
    };
  }, []);


  return (
    <nav className="bg-secondary-content p-4">
      <div className="container mx-auto flex items-center justify-between">
        <div className="flex items-center space-x-4">
          <div className="text-accent font-bold text-xl">
            <Link href="/" >
              App Tournoi
            </Link>
          </div>

        </div>
        <div className="flex items-center space-x-4">
          <a href="/" className="text-white hover:text-accent">
            <RiHomeSmile2Line className="text-2xl" />
          </a>
          <a href="/epreuves" className="text-white hover:text-accent">
            <RiTrophyLine className="text-2xl" />
          </a>
          <a href="/liste" className="text-white hover:text-accent">
            Liste
          </a>
          <a href="#" className="text-white hover:text-accent">
            Contact
          </a>
          {!isUserLogin && (
            <div className="flex items-center space-x-4">
              {/* <a href="/inscription" >
              <button className="btn btn-sm btn-secondary text-2md"><RiAccountPinCircleLine />S'inscrire</button>
              </a>
              <a href="/connexion" >
              <button className="btn btn-sm btn-accent text-2md"><RiLoginCircleLine />Se connecter</button>
              </a> */}
              <LogInButton />
            </div>
          )}
          {isUserLogin && (
            <div className="flex items-center space-x-4">
              <div className="flex items-center space-x-4">
                <button className="bg-primary hover:bg-secondary text-white font-bold py-2 px-4 rounded" onClick={onAddClick}>
                  <RiUserAddLine className="text-xl" />
                </button>
                <div className="relative group" ref={menuRef}>
                  <Image
                    width={32} height={32}
                    src="/assets/images/noavatar.jpg"
                    alt="Avatar"
                    className="rounded-full h-8 w-8 object-cover cursor-pointer"
                    onClick={handleAvatarClick}
                  />
                  {isSubMenuOpen && (
                    <div className="absolute right-0 mt-2 bg-white text-gray-800 rounded-md shadow-md py-2 space-y-2">
                      <a href="#" className="block px-4 py-2 hover:bg-gray-200">
                        <RiAccountCircleLine className="text-2xl" />
                      </a>
                      <a href="#" className="block px-4 py-2 hover:bg-gray-200">
                        <RiLogoutCircleLine className="text-2xl" />
                      </a>
                    </div>
                  )}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </nav>
  )
};

export default Navbar;