// components/HeroSection.js
const HeroSection = () => {
    return (
      <section className="bg-primary-content text-white py-20">
        <div className="container mx-auto text-center">
          <h1 className="text-4xl font-bold mb-4">Welcome to Your App</h1>
          <p className="text-lg mb-8">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          <a href="#" className="bg-white text-secondary font-bold py-2 px-6 rounded-full hover:bg-gray-200">Get Started</a>
        </div>
      </section>
    );
  };
  
  export default HeroSection;