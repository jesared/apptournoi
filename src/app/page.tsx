import FeaturesSection from "@/components/features/feature-section";
import ContactForm from "@/components/forms/contact-form";
import HeroSection from "@/components/heros/hero-sections";


export default function Home() {


  return (
    // navbar
    <>
    <div className="font-sans bg-gray-100">

      
      <FeaturesSection />
      <HeroSection />
      <ContactForm />
    </div>
      
    </>
    // contenu

    // footer
  )
}
