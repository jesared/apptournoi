

import LogInButton from "@/components/authentification/login-button"
import { User } from "@/components/authentification/user";
import { authConfig } from "@/pages/api/auth/[...nextauth]";
import { getServerSession } from 'next-auth'

export default async function AccountUser () {

  const session = await getServerSession(authConfig);

  if (session) {
    // L'utilisateur est connecté
    return (
      <div className="container mx-auto my-8">
        <User />
      </div>
    )
  } else {
    // L'utilisateur n'est pas connecté
    return (
      <div className="container mx-auto my-8">
        <h1 className="text-2xl font-bold mb-4">Connectez-vous, mon ami ...</h1>
        <LogInButton />
      </div>
    )
  }
}